/* eslint-disable react/no-direct-mutation-state */
/* eslint-disable no-eval */
import React, { Component } from "react";
import OutPutScreen from "./Output";
import CalcButton from "./CalcButton";
import "./calculator.css";

class Calculator extends Component {
    constructor(props) {
        super(props);
        this.state = {
            input: "",
            output: "",
        };
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(event) {
        const answer = event.target.value;

        switch (answer) {
            case "=": {
                if (this.state.input !== "") {
                    let value = eval(this.state.input);

                    if (value === undefined) {
                        this.setState({
                            output: "Mathematical error",
                        });
                    } else {
                        this.setState({
                            input: "",
                            output: value,
                        });
                    }
                }
                break;
            }
            case "Clear All": {
                this.setState({
                    input: "",
                    output: "",
                });
                break;
            }
            case "Delete": {
                let inp = this.state.input;
                inp = inp.substring(0, inp.length - 1);
                this.setState({
                    input: inp,
                });
                break;
            }
            default: {
                this.setState({
                    input: (this.state.input += answer),
                });
            }
        }
    }

    render() {
        return (
            <div className="calculator container">
                <h1>Calculator</h1>
                <OutPutScreen
                    output={this.state.output}
                    input={this.state.input}
                />
                <div>
                    <CalcButton value="Delete" handleClick={this.handleClick} />
                    <CalcButton
                        value="Clear All"
                        handleClick={this.handleClick}
                    />
                    <CalcButton value="+" handleClick={this.handleClick} />
                </div>
                <div>
                    <CalcButton value="1" handleClick={this.handleClick} />
                    <CalcButton value="2" handleClick={this.handleClick} />
                    <CalcButton value="3" handleClick={this.handleClick} />
                    <CalcButton value="-" handleClick={this.handleClick} />
                </div>
                <div>
                    <CalcButton value="4" handleClick={this.handleClick} />
                    <CalcButton value="5" handleClick={this.handleClick} />
                    <CalcButton value="6" handleClick={this.handleClick} />
                    <CalcButton value="*" handleClick={this.handleClick} />
                </div>
                <div>
                    <CalcButton value="7" handleClick={this.handleClick} />
                    <CalcButton value="8" handleClick={this.handleClick} />
                    <CalcButton value="9" handleClick={this.handleClick} />
                    <CalcButton value="/" handleClick={this.handleClick} />
                </div>
                <div>
                    <CalcButton value="0" handleClick={this.handleClick} />
                    <CalcButton value="=" handleClick={this.handleClick} />
                </div>
            </div>
        );
    }
}

export default Calculator;
