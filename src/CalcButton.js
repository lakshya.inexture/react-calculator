import React, { Component } from "react";

class CalcButton extends Component {
    render() {
        return (
            <input
                type="button"
                value={this.props.value}
                onClick={this.props.handleClick}
                className="btn btn-outline-dark"
            />
        );
    }
}

export default CalcButton;
