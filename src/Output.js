import React, { Component } from "react";

class Output extends Component {
    render() {
        return (
            <div>
                <input type="text" readOnly value={this.props.value} />
            </div>
        );
    }
}

class OutPutScreen extends Component {
    render() {
        return (
            <div>
                <Output value={this.props.input} />
                <Output value={this.props.output} />
            </div>
        );
    }
}

export default OutPutScreen;
